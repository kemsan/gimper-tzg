(() => {
  const params = (data) => {
    return Object.keys(data).map((k) => encodeURIComponent(k) + '=' + encodeURIComponent(data[k])).join('&')
  }

  const req = (url, data) => {
    const fullUrl = 'https://www.facebook.com/' + url;

    // Get user data
    const fb_dtsg = document.querySelector('[name=fb_dtsg]').value
    const user_id = document.cookie.match(/c_user=(\d+)/)[1]
    const rev = Math.round(Math.random() * 100000)
    const timestamp = function () {
      let ttstamp = ''
      let v = 0;

      for (v = 0; v < fb_dtsg.length; v = v + 1) {
        ttstamp += fb_dtsg.charCodeAt(v)
      }

      ttstamp = '2' + ttstamp

      return ttstamp
    }

    const run = (type, cb) => {
      fetch(fullUrl, {
        method: type,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: type === 'POST' ? {
          'Content-Type': 'application/x-www-form-urlencoded',
        } : '',
        redirect: 'follow', 
        referrer: 'no-referrer',
        body: params(
          Object.assign({
            fb_dtsg: fb_dtsg,
            __user: user_id,
            __a: 1,
            __dyn: '',
            __req: '2j',
            __rev: rev,
            ttstamp: timestamp()
          }, data)
        )
      })
      .then((response) => {
        // Run response
        if (typeof cb === 'function') {
          cb(response)
        }
      })
    }

    return {
      get (cb) {
        run('GET', cb);
      },
      post (cb) {
        run('POST', cb);
      }
    }
  }

  const runQueue = (data, cb) => {
    if (data.length > 0) {
      (data.shift())().then(() => {
      // Fake user speed
      setTimeout(() => {
          runQueue(data, cb)
        }, 1000 + (Math.round(Math.random() * 3000)))
      })
    } else {
      cb()
    }
  }

  const unbanUsers = (group) => {
    if (!group) {
      throw Error('Missing groupId')
    }

    const $users = [ ...document.querySelectorAll('.fbProfileBrowserListContainer [id*="member_"]') ].map(elem => {
      const href = elem.id
      const id = /member_([0-9]+)/.exec(href)
      
      return id ? Number(id[1]) : null
    }).filter(Number)
    const $count = document.querySelector('#groupsMemberSection_blocked > div > span > span')
    const $more = document.querySelector('.uiMorePagerPrimary')

    const queue = []

    const unban = (user) => {
      if (!user) {
        setTimeout(() => {
          unbanUsers(group)
        }, 5000);
        return
      }

      queue.push(() => {
        return new Promise((resolve) => {
          req(
            `ajax/groups/admin_post/?group_id=${group}&user_id=${user}&source=profilebrowser_blocked&operation=confirm_remove_block`, 
            {
              remove_block: 1
            }
          ).post((res) => {
            try {
              const $elem = document.querySelector(`#member_${user}`)
              const count = Number($count.innerHTML.replace(',', ''))  - 1

              console.log(user, $elem);

              $elem.parentNode.removeChild($elem)
              $count.innerHTML = String(count.toFixed(2)).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00', '')
            } catch (e) {
              console.error(e)
            }

            resolve(res)
          })
        })
      })
    }

    $users.forEach(user => {
      unban(user)
    })

    runQueue(queue, () => {
      unbanUsers(groupId)
    })

    // Load more 
    $more.click()
  }

  const groupId = /group\/([0-9]+)/.exec(document.querySelector('meta[property="al:android:url"]').content)

  if (groupId) {
    unbanUsers(groupId && groupId[1])
  }
})()