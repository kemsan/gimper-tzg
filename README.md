# Gimper TZG

1. Wejdź na grupę
2. Przejdź na stronę z listą Użytkowników
3. Przejdź na podstronę z zablokowanymi użytkownikami
4. Wciśnij **CTRL + SHIFT + I** lub **⌘ + Opcja + j**
5. Przejdź na zakładkę **Console**
6. Przeklej kod zawarty w pliku **script.js**
7. Wciśnij enter
8. Proces odbanowywania powinien się rozpocząć - liczba osób zbanowanych powinna maleć, a użytkownicy z listy powinni być usuwani
9. Sprawdź co ok 7-10 min czy proces nie został przerwany
